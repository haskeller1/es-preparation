{% set customer=salt['pillar.get']('analytics-cloud:customer') %}
{% set es_master_node=salt['pillar.get']('analytics-cloud:es_master_node') %}

analytics-dir:
  file.directory:
    - name: /opt/mistnet/analytics
    - user: root
    - group: root
    - dir_mode: 755
    - makedirs: True

/opt/mistnet/analytics/init_indices.sh:
  file.managed:
    - source: salt://es-preparation/init_indices.sh
    - user: root
    - group: root
    - mode: 700
    - template: jinja

/opt/mistnet/analytics/create_daily_index.sh:
  file.managed:
    - source: salt://es-preparation/create_daily_index.sh
    - user: root
    - group: root
    - mode: 700
    - template: jinja

/opt/mistnet/analytics/create_template.sh:
  file.managed:
    - source: salt://es-preparation/create_template.sh
    - user: root
    - group: root
    - mode: 700
    - template: jinja

/opt/mistnet/analytics/create_template_moloch.sh:
  file.managed:
    - source: salt://es-preparation/create_template_moloch.sh
    - user: root
    - group: root
    - mode: 700
    - template: jinja

/opt/mistnet/analytics/template-{{customer}}.txt:
  file.managed:
    - source: salt://es-preparation/template.txt
    - user: root
    - group: root
    - mode: 600
    - template: jinja

/opt/mistnet/analytics/template-moloch-{{customer}}.txt:
  file.managed:
    - source: salt://es-preparation/template-moloch.txt
    - user: root
    - group: root
    - mode: 600
    - template: jinja

/opt/mistnet/analytics/create_geoip_processor.sh:
  file.managed:
    - source: salt://es-preparation/create_geoip_processor.sh
    - user: root
    - group: root
    - mode: 700

/opt/mistnet/analytics/geoip-processor.txt:
  file.managed:
    - source: salt://es-preparation/geoip-processor.txt
    - user: root
    - group: root
    - mode: 600

create_daily_index:
  cron.present:
    - name: /opt/mistnet/analytics/create_daily_index.sh {{customer}} {{es_master_node}}:9200
    - user: root
    - hour: 23
    - minute: 30

curl:
  pkg.installed

elasticsearch_dir:
  cmd.run:
    - name: |
        /opt/mistnet/analytics/init_indices.sh {{es_master_node}}:9200 
        /opt/mistnet/analytics/create_template.sh {{es_master_node}}:9200 /opt/mistnet/analytics/template-{{customer}}.txt
        /opt/mistnet/analytics/create_template_moloch.sh {{es_master_node}}:9200 /opt/mistnet/analytics/template-moloch-{{customer}}.txt
        /opt/mistnet/analytics/create_geoip_processor.sh {{es_master_node}}:9200 /opt/mistnet/analytics/geoip-processor.txt
