#!/bin/bash
# Bash script to set index template
# By Ray Qiu <ray@mistnet.io>, 2017

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <es-cluster-uri> <template-file>"
  exit 1
fi

ES_URI=$1
TEMPLATE_FILE=$2

curl -XPUT "http://$ES_URI/_template/default"  -H 'Content-Type: application/json' -d @$TEMPLATE_FILE
