#!/bin/bash
# Bash script to create daily indices and aliases
# By Ray Qiu <ray@mistnet.io>, 2017

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <customer-name> <es-cluster-uri>"
  exit 1
fi

CUSTOMER=$1
ES_URI=$2

DATE=`date +"%Y.%m.%d" --date "1 hours"`
CASE_INDEX="$CUSTOMER-cases"
IOA_INDEX="$CUSTOMER-ioas"

curl -XPUT http://$ES_URI/"$CASE_INDEX"  -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : 5,
            "number_of_replicas" : 1,
            "translog": {
                "durability": "async"
            },
            "mapping" : {
                "total_fields" : {
                    "limit" : "4000"
                }
            }
        }
    }
}'

curl -XPUT http://$ES_URI/"$IOA_INDEX"  -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : 5,
            "number_of_replicas" : 1,
            "translog": {
                "durability": "async"
            },
            "mapping" : {
                "total_fields" : {
                    "limit" : "4000"
                }
            }
        }
    }
}'

