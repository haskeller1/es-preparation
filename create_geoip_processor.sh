#!/bin/bash
# Bash script to create geoip processor
# By Ray Qiu <ray@mistnet.io>, 2017

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <es-cluster-uri> <data-file>"
  exit 1
fi

ES_URI=$1
DATA_FILE=$2

curl -XPUT "http://$ES_URI/_ingest/pipeline/geoip"  -H 'Content-Type: application/json' -d @$DATA_FILE
