#!/bin/bash
# Bash script to create daily indices and aliases
# By Ray Qiu <ray@mistnet.io>, 2017
{% set es_shards=salt['pillar.get']('analytics-cloud:es_shards', 2) %}
{% set replica_shards=salt['pillar.get']('analytics-cloud:replica_shards', 0) %}
{% set log_retention_days=salt['pillar.get']('analytics-cloud:log_retention_days', 7) %}
{% set event_retention_days=salt['pillar.get']('analytics-cloud:event_retention_days', 14) %}
{% set uba_retention_days=salt['pillar.get']('analytics-cloud:uba_retention_days', 60) %}

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <customer-name> <es-cluster-uri>"
  exit 1
fi

CUSTOMER=$1
ES_URI=$2

DATE=`date +"%Y.%m.%d" --date "1 hours"`
RAWLOG_INDEX="$CUSTOMER-raw-logs-$DATE"
UBA_INDEX="$CUSTOMER-uba-$DATE"
RAWEVENT_INDEX="$CUSTOMER-raw-events-$DATE"
DEDUPEVENT_INDEX="$CUSTOMER-deduped-events-$DATE"
EVENTLOG_INDEX="$CUSTOMER-event-logs-$DATE"

curl -XPUT http://"$ES_URI"/"$UBA_INDEX"

curl -XPUT http://$ES_URI/"$RAWLOG_INDEX"  -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : {{es_shards}}, 
            "number_of_replicas" : {{replica_shards}}, 
            "refresh_interval": "30s",
            "translog": {
                "durability": "async"
            }
        }
    }
}'

curl -XPUT http://$ES_URI/"$RAWEVENT_INDEX"  -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : {{es_shards}},
            "translog": {
                "durability": "async"
            } 
        }
    }
}'

curl -XPUT http://$ES_URI/"$EVENTLOG_INDEX"  -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : {{es_shards}},
            "translog": {
                "durability": "async"
            }
        }
    }
}'

OLDDATE1=`date +"%Y.%m.%d" --date "7 days ago"`
OLDDATE2=`date +"%Y.%m.%d" --date "14 days ago"`
YESTERDAY=`date +"%Y.%m.%d" --date "2 days ago"`
TDBY=`date +"%Y.%m.%d" --date "3 days ago"`
DELETE_DATE=`date +"%Y.%m.%d" --date "{{log_retention_days}} days ago"`
EVENT_DELETE_DATE=`date +"%Y.%m.%d" --date "{{event_retention_days}} days ago"`
UBA_DELETE_DATE=`date +"%Y.%m.%d" --date "{{uba_retention_days}} days ago"`

curl -XPOST "http://$ES_URI/_aliases?pretty"  -H 'Content-Type: application/json' -d'
{
    "actions" : [
        { "add" : { "index" : "'$RAWEVENT_INDEX'", "alias" : "'$CUSTOMER'-last7-raw-events" } },
        { "add" : { "index" : "'$RAWEVENT_INDEX'", "alias" : "'$CUSTOMER'-last14-raw-events" } },
        { "add" : { "index" : "'$RAWEVENT_INDEX'", "alias" : "'$CUSTOMER'-last2-raw-events" } },
        { "add" : { "index" : "'$RAWLOG_INDEX'", "alias" : "'$CUSTOMER'-last7-raw-logs" } },
        { "add" : { "index" : "'$RAWLOG_INDEX'", "alias" : "'$CUSTOMER'-last14-raw-logs" } },
        { "add" : { "index" : "'$RAWLOG_INDEX'", "alias" : "'$CUSTOMER'-last2-raw-logs" } },
        { "add" : { "index" : "'$EVENTLOG_INDEX'", "alias" : "'$CUSTOMER'-last7-event-logs" } },
        { "add" : { "index" : "'$EVENTLOG_INDEX'", "alias" : "'$CUSTOMER'-last14-event-logs" } },
        { "add" : { "index" : "'$EVENTLOG_INDEX'", "alias" : "'$CUSTOMER'-last2-event-logs" } }
    ]
}'

curl -XPOST "http://$ES_URI/_aliases?pretty"  -H 'Content-Type: application/json' -d'
{
    "actions" : [
        { "remove" : { "index" : "'$CUSTOMER'-raw-events-'$YESTERDAY'", "alias" : "'$CUSTOMER'-last2-raw-events" } },
        { "remove" : { "index" : "'$CUSTOMER'-raw-logs-'$YESTERDAY'", "alias" : "'$CUSTOMER'-last2-raw-logs" } },
        { "remove" : { "index" : "'$CUSTOMER'-event-logs-'$YESTERDAY'", "alias" : "'$CUSTOMER'-last2-event-logs" } }
    ]
}'

curl -XPOST "http://$ES_URI/_aliases?pretty"  -H 'Content-Type: application/json' -d'
{
    "actions" : [
        { "remove" : { "index" : "'$CUSTOMER'-raw-events-'$OLDDATE1'", "alias" : "'$CUSTOMER'-last7-raw-events" } },
        { "remove" : { "index" : "'$CUSTOMER'-raw-logs-'$OLDDATE1'", "alias" : "'$CUSTOMER'-last7-raw-logs" } },
        { "remove" : { "index" : "'$CUSTOMER'-event-logs-'$OLDDATE1'", "alias" : "'$CUSTOMER'-last7-event-logs" } }
    ]
}'

curl -XPOST "http://$ES_URI/_aliases?pretty"  -H 'Content-Type: application/json' -d'
{
    "actions" : [
        { "remove" : { "index" : "'$CUSTOMER'-raw-events-'$OLDDATE2'", "alias" : "'$CUSTOMER'-last14-raw-events" } },
        { "remove" : { "index" : "'$CUSTOMER'-raw-logs-'$OLDDATE2'", "alias" : "'$CUSTOMER'-last14-raw-logs" } },
        { "remove" : { "index" : "'$CUSTOMER'-event-logs-'$OLDDATE2'", "alias" : "'$CUSTOMER'-last14-event-logs" } }
    ]
}'

# Enable replica shards for old raw-logs indices
curl -XPUT http://$ES_URI/$CUSTOMER-raw-logs-$YESTERDAY/_settings -H 'Content-Type: application/json' -d'
{
  "number_of_replicas" : 1,
  "refresh_interval": "5s"
}'

# Delete old data
curl -XDELETE http://$ES_URI/$CUSTOMER-basic-artifacts-$YESTERDAY
curl -XDELETE http://$ES_URI/$CUSTOMER-raw-logs-$DELETE_DATE
curl -XDELETE http://$ES_URI/$CUSTOMER-raw-events-$EVENT_DELETE_DATE
curl -XDELETE http://$ES_URI/$CUSTOMER-event-logs-$EVENT_DELETE_DATE
curl -XDELETE http://$ES_URI/$CUSTOMER-scored-events-$TDBY
curl -XDELETE http://$ES_URI/$CUSTOMER-events-$YESTERDAY
curl -XDELETE http://$ES_URI/$CUSTOMER-deduped-events-$YESTERDAY
curl -XDELETE http://$ES_URI/$CUSTOMER-basic-artifacts-$YESTERDAY
curl -XDELETE http://$ES_URI/$CUSTOMER-uba-$UBA_DELETE_DATE

